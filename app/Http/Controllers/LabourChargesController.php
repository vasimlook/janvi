<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
use Session;
use App\Models\LabourCharges;
use DataTables;

class LabourChargesController extends Controller {

    public function index() {
        $data['title'] = 'List-Labour-Charges';
        return view('admin.labourCharges.list', ["data" => $data]);
    }

    public function add() {
        $data['title'] = 'Add-Labour-Charges';
        return view('admin.labourCharges.add', ["data" => $data]);
    }

    public function save(Request $request) {
        DB::table('labour_charges')->insert([
            'name' => $request->name,
            'amount' => $request->amount,
            'added_by' => $request->session()->get('loginId'),
            'is_active' => 1,
            'is_deleted' => 0,
            'date_added' => date("yy-m-d h:i:s"),
            'date_updated' => date("yy-m-d h:i:s")
        ]);
        activity($request,"inserted",'labour-charges');
        successOrErrorMessage("Data added Successfully", 'success');
        return redirect('labour-charges');
    }

    public function list(Request $request) {
        if ($request->ajax()) {
            $data = LabourCharges::latest()->get();
            return Datatables::of($data)
//                            ->addIndexColumn()
                            ->addColumn('index', '')
                            ->editColumn('is_active', function ($row) {
                                $active_inactive_button = '';
                                if ($row->is_active == 1) {
                                    $active_inactive_button = '<span class="badge badge-success">Active</span>';
                                }
                                if ($row->is_active == 0) {
                                    $active_inactive_button = '<span class="badge badge-danger">inActive</span>';
                                }
                                return $active_inactive_button;
                            })
                            ->editColumn('is_deleted', function ($row) {
                                $delete_button = '';
                                if ($row->is_deleted == 1) {
                                    $delete_button = '<span class="badge badge-danger">Deleted</span>';
                                }
                                return $delete_button;
                            })
                            ->addColumn('action', function ($row) {
                                
                                if($row->is_active==1){
                                    $str='<em class="icon ni ni-cross"></em>';
                                    $class="btn-danger";
                                }
                                if($row->is_active==0){
                                    $str='<em class="icon ni ni-check-thick"></em>';
                                    $class="btn-success";
                                }
                                
                                $actionBtn = '<a href="/labour-charges/edit/' . $row->labour_charge_id . '" class="btn btn-xs btn-warning">&nbsp;<em class="icon ni ni-edit-fill"></em></a> <button class="btn btn-xs btn-danger delete_button" data-module="labour-charges" data-id="' . $row->labour_charge_id . '" data-table="labour_charges" data-wherefield="labour_charge_id">&nbsp;<em class="icon ni ni-trash-fill"></em></button> <button class="btn btn-xs '.$class.' active_inactive_button" data-id="' . $row->labour_charge_id . '" data-status="' . $row->is_active . '" data-table="labour_charges" data-wherefield="labour_charge_id" data-module="labour-charges">'.$str.'</button>';
                                return $actionBtn;
                            })
                            ->escapeColumns([])
                            ->make(true);
        }
    }

    public function edit($id) {
        $result = DB::table('labour_charges')->where('labour_charge_id', $id)->first();
        $data['title'] = 'Edit-Labour-Charges';
        $data['result'] = $result;
        return view('admin.labourCharges.edit', ["data" => $data]);
    }

    public function update(Request $request) {
        DB::table('labour_charges')->where('labour_charge_id', $request->id)->update([
            'name' => $request->name,
            'amount' => $request->amount,
            'added_by' => $request->session()->get('loginId'),
            'date_updated' => date("yy-m-d h:i:s")
        ]);
        
        activity($request,"updated",'labour-charges');
        successOrErrorMessage("Data updated Successfully", 'success');
        return redirect('labour-charges');
    }
    public function delete(Request $request) {
        if (isset($_REQUEST['table_id'])) {
            
            $res = DB::table($_REQUEST['table'])->where($_REQUEST['wherefield'], $_REQUEST['table_id'])->update([                                              
                'is_deleted' => 1,                                
                'date_updated' => date("yy-m-d h:i:s")
            ]); 
            activity($request,"deleted",$_REQUEST['module']);
//            $res = DB::table($_REQUEST['table'])->where($_REQUEST['wherefield'], $_REQUEST['table_id'])->delete();
            if ($res) {
                $data = array(
                    'suceess' => true
                );
            } else {
                $data = array(
                    'suceess' => false
                );
            }
            return response()->json($data);
        }
    }
    public function status(Request $request) {       
        if (isset($_REQUEST['table_id'])) {
            
            $res = DB::table($_REQUEST['table'])->where($_REQUEST['wherefield'], $_REQUEST['table_id'])->update([                                              
                'is_active' => $_REQUEST['status'],                                
                'date_updated' => date("yy-m-d h:i:s")
            ]);                        
//            $res = DB::table($_REQUEST['table'])->where($_REQUEST['wherefield'], $_REQUEST['table_id'])->delete();
            if ($res) {
                $data = array(
                    'suceess' => true
                );
            } else {
                $data = array(
                    'suceess' => false
                );
            }
            activity($request,"updated",$_REQUEST['module']);
            return response()->json($data);
        }
    }

}
